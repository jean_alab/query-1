#include <stdio.h>
int main  (int argc, char ** argv)
{
    int nDev;
    cudaGetDeviceCount(&nDev);
    printf("\t CUDA Device Query  \n");
    printf("CUDA devices  %d\n", nDev);
    // Dispositivos
    for (int j = 0; j < nDev; ++j)
    {
        // Obtener propiedades
        printf("\nCUDA Device number: #%d\n", j+1);
        cudaDeviceProp cudaProp;
        cudaGetDeviceProperties(&cudaProp, j);
        printf("Name:                          %s\n",  cudaProp.name);
        for (int i = 0; i < 3; ++i)
        	    printf("Maximum dimension %d of grid:   %d\n", i, cudaProp.maxGridSize[i]);
        for (int i = 0; i < 3; ++i)
        	    printf("Maximum dimension %d of block:  %d\n", i, cudaProp.maxThreadsDim[i]);
        printf("Maximum threads per block:     %d\n",  cudaProp.maxThreadsPerBlock);
        printf("Warp size:                     %d\n",  cudaProp.warpSize);
        printf("Number of multiprocessors:     %d\n",  cudaProp.multiProcessorCount);
        printf("Maximum threads per multiprocessor:     %d\n",  cudaProp.maxThreadsPerMultiProcessor);

    }
    return 0;
}
